import React, {FC, useEffect, useState} from 'react';

interface InputWithSearchProps {
    url: string;
    field: string;
    value: string;
    onClick: (data: object) => void;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    parseData: (data: any) => HTMLLIElement[];
    reset: () => void;
    placeholder: string;
    className: string;
    readOnly: boolean;
}

const InputWithSearch: FC<InputWithSearchProps> = ({url, field, value, onClick, onChange, parseData, reset, placeholder, className, readOnly}) => {
    const [list, setList] = useState(undefined);
    const [drop, setDrop] = useState<boolean>(false);


    useEffect(
        () => {
            window.addEventListener('click', (e) => {console.log(e)});
        }
    );

    return (
        <>
            <input
                className={'search-input ' + className}
                value={value}
                onChange={(e) => {
                    onChange(e);
                }}
                type='text'
                placeholder={placeholder}
                readOnly={readOnly}
                autoComplete='off'
            />
            {
                drop &&
                <section className='search-input__result'>
                    <ul>
                        { list }
                    </ul>
                </section>
            }
        </>
    )
};

InputWithSearch.defaultProps = {
    reset: () => {},
    placeholder: 'Search',
    className: 'search-input',
    readOnly: false
};

export default InputWithSearch;
